import request from "@/utils/request.js";

export function listBrand(query) {
    return request({
        url: '/product/brand/list',
        method: 'get',
        params: query
    })
}

export function addBrand(data) {
    return request({
        url: '/product/brand/add',
        method: 'post',
        data
    })
}

export function getBrand(id) {
    return request({
        // url: '/product/brand/ + id',
        url: `/product/brand/${id}`,
        method: 'get'
    })
}

export function updateBrand(data) {
    return request({
        url: '/product/brand/edit',
        method: 'put',
        data
    })
}

// 删除品牌
export function deleteBrand(ids) {
    return request({
        url: `/product/brand/${ids}`,
        method: 'delete'
    })
}

// 获取全部品牌
export function getBrandAll() {
    return request({
        url: '/product/brand/getBrandAll',
        method: 'get'
    })
}