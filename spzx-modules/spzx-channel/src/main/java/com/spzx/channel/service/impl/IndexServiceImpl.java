package com.spzx.channel.service.impl;

import com.spzx.channel.service.IIndexService;
import com.spzx.common.core.constant.SecurityConstants;
import com.spzx.common.core.domain.R;
import com.spzx.common.core.exception.ServiceException;
import com.spzx.product.api.RemoteCategoryService;
import com.spzx.product.api.RemoteProductService;
import com.spzx.product.api.domain.ProductSku;
import com.spzx.product.api.domain.vo.CategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IndexServiceImpl implements IIndexService {

    @Autowired
    private RemoteCategoryService remoteCategoryService;

    @Autowired
    private RemoteProductService remoteProductService;

    @Override
    public Map<String, Object> getIndexData() {
        R<List<CategoryVo>> oneCategoryResult = remoteCategoryService.getOneCategory(SecurityConstants.INNER);
        if (oneCategoryResult.getCode() == R.FAIL) {
            throw new ServiceException(oneCategoryResult.getMsg());
        }
        R<List<ProductSku>> topSaleListResult = remoteProductService.getTopSale(SecurityConstants.INNER);
        if (topSaleListResult.getCode() == R.FAIL) {
            throw new ServiceException(topSaleListResult.getMsg());
        }
        Map<String, Object> map = new HashMap();
        map.put("categoryList", oneCategoryResult.getData());
        map.put("productSkuList", topSaleListResult.getData());
        return map;
    }
}
