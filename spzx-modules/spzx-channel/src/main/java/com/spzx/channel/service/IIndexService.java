package com.spzx.channel.service;

import java.util.List;
import java.util.Map;

public interface IIndexService {
    Map<String, Object> getIndexData() ;
}
