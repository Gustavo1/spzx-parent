package com.spzx.user.mapper;

import java.util.List;

import com.spzx.user.domain.UserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户地址Mapper接口
 *
 * @author atguigu
 * @date 2024-07-10
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {
    
    
}
