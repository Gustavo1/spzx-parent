package com.spzx.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spzx.product.api.domain.vo.CategoryVo;
import com.spzx.product.domain.Category;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 商品分类Service接口
 */
public interface ICategoryService extends IService<Category> {

    /**
     * 获取分类下拉树列表
     *
     * @param id
     * @return
     */
    List<Category> treeSelect(Long id);

    /**
     * 根据当前分类id，查询当前分类id和所有父级id，封装在ArrayList中
     *
     * @param id 当前分类id
     * @return 当前分类id和所有父级id集合
     */
    List<Long> getAllParentId(Long id);

    /**
     * 采用递归，根据指定的分类id获取当前类和当前类父类的分类对象集合
     * @param id 当前分类id
     * @param categoryList 自带数据容器，方法返回的数据放在此容器
     * @return 当前分类id和所有父级id集合
     */
    // List<Category> getParentIdList(Long id, List<Category> categoryList);

    /**
     * 分类导出Excel
     *
     * @param response
     */
    void exportData(HttpServletResponse response);

    /**
     * 导入数据到数据库里
     *
     * @param file 上传的文件
     */
    void importData(MultipartFile file);

    /**
     * 查询首页所有1级分类
     *
     * @return
     */
    List<CategoryVo> getOneCategory();

    /**
     * 查询所有分类树结构
     *
     * @return 所有一级分类（一级分类有children集合分类属性）
     */
    List<CategoryVo> tree();
}