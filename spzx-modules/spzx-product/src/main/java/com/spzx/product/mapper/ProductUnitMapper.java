package com.spzx.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.spzx.product.domain.ProductUnit;
import org.apache.ibatis.annotations.Param;

/**
 * 商品单位Mapper接口
 */
public interface ProductUnitMapper extends BaseMapper<ProductUnit> {

    
    IPage<ProductUnit> findPage(Page<ProductUnit> pageParam,@Param("query") ProductUnit productUnit);
}