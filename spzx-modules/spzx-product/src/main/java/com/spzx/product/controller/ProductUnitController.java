package com.spzx.product.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.spzx.common.core.web.controller.BaseController;
import com.spzx.common.core.web.domain.AjaxResult;
import com.spzx.common.core.web.page.TableDataInfo;
import com.spzx.common.security.utils.SecurityUtils;
import com.spzx.product.domain.ProductUnit;
import com.spzx.product.service.IProductUnitService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 商品单位Controller
 */
@Tag(name = "商品单位接口管理")
@RestController
@RequestMapping("/productUnit")
public class ProductUnitController extends BaseController {

    @Autowired
    private IProductUnitService productUnitService;

    @Operation(summary = "商品单位分页查询")
    @GetMapping("/list")
    public TableDataInfo list(
            @Parameter(name = "pageNum", description = "当前页", required = false)
            @RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
            @Parameter(name = "pageSize", description = "每页页数", required = false)
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @Parameter(name = "productUnit", description = "查询条件", required = false)
            ProductUnit productUnit) {
        Page<ProductUnit> pageParam = new Page<>(pageNum, pageSize);
        IPage<ProductUnit> page = productUnitService.findPage(pageParam, productUnit);
        return getDataTable(page);
    }

    @Operation(summary = "通过id获取商品单位详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getProductUnitById(@PathVariable("id") Integer id) {
        return AjaxResult.success(productUnitService.getProductUnitById(id));
    }
    
    @Operation(summary = "新增商品单位")
    @PostMapping
    public AjaxResult add(@RequestBody @Validated ProductUnit productUnit) {
        productUnit.setCreateBy(SecurityUtils.getUsername());
        return toAjax(productUnitService.insertProductUnit(productUnit));
    }

    /**
     * 修改商品单位
     */
    @Operation(summary = "修改商品单位")
    @PutMapping
    public AjaxResult edit(@RequestBody @Validated ProductUnit productUnit) {
        productUnit.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(productUnitService.updateProductUnit(productUnit));
    }

    /**
     * 删除商品单位
     */
    @Operation(summary = "删除商品单位")
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productUnitService.deleteProductUnitByIds(ids));
    }

    @Operation(summary = "获取全部单位")
    @GetMapping("getUnitAll")
    public AjaxResult selectProductUnitAll() {
        return success(productUnitService.list());
    }

}
