package com.spzx.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spzx.product.domain.Brand;
import com.spzx.product.domain.CategoryBrand;

import java.util.List;

/**
 * 分类品牌Service接口
 */
public interface ICategoryBrandService extends IService<CategoryBrand> {

    /**
     * 查询分类品牌列表
     * @param categoryBrand 分类品牌
     * @return 分类品牌集合
     */
    List<CategoryBrand> selectCategoryBrandList(CategoryBrand categoryBrand);

    /**
     * 根据分类id查询详情
     * @param id 当前分类级别id
     * @return
     */
    CategoryBrand selectCategoryBrandById(Long id);

    /**
     * 添加一个CategoryBrand
     * @param categoryBrand 
     * @return 结果
     */
    int insertCategoryBrand(CategoryBrand categoryBrand);

    /**
     * 修改CategoryBrand
     * @param categoryBrand
     * @return 结果
     */
    int updateCategoryBrand(CategoryBrand categoryBrand);

    /**
     * 删除CategoryBrand
     * @param ids
     * @return
     */
    int deleteCategoryBrand(Long[] ids);

    /**
     * 扩展接口：服务于商品添加表单页面等
     * 根据分类id获取品牌列表
     * @param categoryId 分类id
     * @return 品牌列表
     */
    List<Brand> selectBrandListByCategoryId(Long categoryId);
}