package com.spzx.product.mapper;

import com.spzx.product.domain.Brand;

import java.util.List;

/**
 * 品牌Mapper接口
 *
 */
public interface BrandMapper {

    /**
     * 品牌列表分页查询
     * @param brand 查询条件
     * @return 列表数据
     */
    List<Brand> selectBrandList(Brand brand);
    
    /**
     * 根据id查询品牌详细信息
     * @param id 主键
     * @return 实体对象
     */
    Brand selectBrandById(Long id);

    /**
     * 保存品牌
     * @param brand 新增品牌
     * @return 影响数量
     */
    int insertBrand(Brand brand);

    /**
     * 修改品牌
     * @param brand 品牌
     * @return 影响数量
     */
    int updateBrand(Brand brand);

    /**
     * 批量删除品牌信息
     * @param ids 需要删除的品牌ID
     * @return 结果
     */
    int deleteBrandByIds(Long[] ids);
    
}
