package com.spzx.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spzx.common.core.exception.ServiceException;
import com.spzx.product.domain.Brand;
import com.spzx.product.domain.Category;
import com.spzx.product.domain.CategoryBrand;
import com.spzx.product.mapper.CategoryBrandMapper;
import com.spzx.product.service.ICategoryBrandService;
import com.spzx.product.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * 分类品牌Service业务层处理
 */
@Service
public class CategoryBrandServiceImpl extends ServiceImpl<CategoryBrandMapper, CategoryBrand> implements ICategoryBrandService {

    @Autowired
    private CategoryBrandMapper categoryBrandMapper;

    @Autowired
    private ICategoryService categoryService;

    @Override
    public List<CategoryBrand> selectCategoryBrandList(CategoryBrand categoryBrand) {
        return categoryBrandMapper.selectCategoryBrandList(categoryBrand);
    }

    @Override
    public CategoryBrand selectCategoryBrandById(Long id) {
        // 根据id获取当前 `分类品牌`
        CategoryBrand categoryBrand = categoryBrandMapper.selectById(id);
        // 解决分类下拉回显问题，结果是当前分类id和当前分类的上级id
        List<Long> categoryIdList = categoryService.getAllParentId(categoryBrand.getCategoryId());
        // 将 `分类id集合` 赋值给 `分类品牌对象`
        categoryBrand.setCategoryIdList(categoryIdList);
        return categoryBrand;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertCategoryBrand(CategoryBrand categoryBrand) {
        long count = categoryBrandMapper.selectCount(
                new LambdaQueryWrapper<CategoryBrand>()
                        .eq(CategoryBrand::getCategoryId, categoryBrand.getCategoryId())
                        .eq(CategoryBrand::getBrandId, categoryBrand.getBrandId())
        );
        if (count > 0) {
            throw new ServiceException("已存在该分类");
        }
        return categoryBrandMapper.insert(categoryBrand);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateCategoryBrand(CategoryBrand categoryBrand) {
        // // 获取原始CategoryBrand
        // CategoryBrand originalCategoryBrand = this.getById(categoryBrand.getId());
        // // 1.关联数据不变，无需修改
        // if (originalCategoryBrand.getCategoryId() == categoryBrand.getCategoryId() && originalCategoryBrand.getBrandId() == categoryBrand.getBrandId()) {
        //     return 1;
        // }
        // // 2.修改的分类和品牌关联数据已存在
        // Long count = categoryBrandMapper.selectCount(
        //         new LambdaQueryWrapper<CategoryBrand>()
        //                 .eq(CategoryBrand::getCategoryId, categoryBrand.getCategoryId())
        //                 .eq(CategoryBrand::getBrandId, categoryBrand.getBrandId())
        // );
        // if (count > 0) {
        //     throw new ServiceException("修改的分类和品牌关联数据已存在");
        // }
        // return categoryBrandMapper.updateById(categoryBrand);

        CategoryBrand originalCategoryBrand = this.getById(categoryBrand.getId());
        if (categoryBrand.getCategoryId().longValue() != originalCategoryBrand.getCategoryId().longValue()
                || categoryBrand.getBrandId().longValue() != originalCategoryBrand.getBrandId().longValue()) {
            long count = categoryBrandMapper.selectCount(
                    new LambdaQueryWrapper<CategoryBrand>()
                            .eq(CategoryBrand::getCategoryId, categoryBrand.getCategoryId())
                            .eq(CategoryBrand::getBrandId, categoryBrand.getBrandId()));
            if (count > 0) {
                throw new ServiceException("该分类已加该品牌");
            }
        }
        return categoryBrandMapper.updateById(categoryBrand);
    }

    @Override
    public int deleteCategoryBrand(Long[] ids) {
        return categoryBrandMapper.deleteBatchIds(Arrays.asList(ids));
    }

    @Override
    public List<Brand> selectBrandListByCategoryId(Long categoryId) {
        return categoryBrandMapper.selectBrandListByCategoryId(categoryId);
    }
}