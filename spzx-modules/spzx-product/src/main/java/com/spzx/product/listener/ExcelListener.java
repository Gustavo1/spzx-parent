package com.spzx.product.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import org.apache.poi.ss.formula.functions.T;
import org.aspectj.weaver.ast.Var;

import java.util.ArrayList;
import java.util.List;

public class ExcelListener<T> extends AnalysisEventListener<T> {

    List<T> data = new ArrayList<T>();

    @Override
    public void invoke(T t, AnalysisContext analysisContext) {
        data.add(t);
        System.out.println("============================data = " + data);
    }

    public List<T> getData() {
        return data;
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        System.out.println("============================data = " + data);
    }
}
