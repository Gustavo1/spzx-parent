package com.spzx.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spzx.product.api.domain.ProductSku;
import com.spzx.product.domain.*;
import com.spzx.product.mapper.ProductDetailsMapper;
import com.spzx.product.mapper.ProductMapper;
import com.spzx.product.mapper.SkuStockMapper;
import com.spzx.product.mapper.productSkuMapper;
import com.spzx.product.service.IProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 商品Service业务层处理
 */
@Slf4j
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private productSkuMapper productSkuMapper;

    @Autowired
    private ProductDetailsMapper productDetailsMapper;

    @Autowired
    private SkuStockMapper skuStockMapper;

    @Override
    public List<Product> selectProductList(Product product) {
        return productMapper.selectProductList(product);
    }

    @Transactional
    @Override
    public int insertProduct(Product product) {
        // 1.保存Product
        productMapper.insert(product);  // 注意：主键回填

        // 2.保存ProductSku
        List<ProductSku> productSkuList = product.getProductSkuList();
        int size = productSkuList.size();
        for (int i = 0; i < size; i++) {
            ProductSku productSku = productSkuList.get(i);
            productSku.setSkuCode(product.getId() + "_" + i);
            productSku.setSkuName(product.getName() + " " + productSku.getSkuSpec());
            productSku.setProductId(product.getId());
            productSku.setStatus(0);
            productSkuMapper.insert(productSku);

            // 3.保存SkuStock
            SkuStock skuStock = new SkuStock();
            skuStock.setSkuId(productSku.getId());
            skuStock.setTotalNum(productSku.getStockNum());
            skuStock.setLockNum(0);
            skuStock.setAvailableNum(productSku.getStockNum());
            skuStock.setSaleNum(0);
            skuStockMapper.insert(skuStock);
        }

        // 4.保存ProductDetails
        ProductDetails productDetails = new ProductDetails();
        productDetails.setProductId(product.getId());
        productDetails.setImageUrls(String.join(",", product.getDetailsImageUrlList()));
        productDetailsMapper.insert(productDetails);

        return 1;
    }

    @Override
    public Product selectProductById(Long id) {
        // 1.查询Product对象，将其他数据都封装在product中一起返回
        Product product = productMapper.selectById(id);

        // 2.根据商品ip查询List<ProductSku>
        List<ProductSku> productSkuList = productSkuMapper.selectList(new LambdaQueryWrapper<ProductSku>().eq(ProductSku::getProductId, id));
        product.setProductSkuList(productSkuList);

        List<Long> productSkuIdList = productSkuList.stream().map(ProductSku::getId).collect(Collectors.toList());

        // 根据productSkuIdList集合，查询SkuStock集合
        List<SkuStock> skuStockList = skuStockMapper.selectList(new LambdaQueryWrapper<SkuStock>().in(SkuStock::getSkuId, productSkuIdList));
        Map<Long, Integer> skuToTotalNumMap = skuStockList.stream().collect(Collectors.toMap(SkuStock::getSkuId, SkuStock::getTotalNum));

        productSkuList.forEach(productSku -> {
            // 3.查询每一个sku的库存数据
            productSku.setStockNum(skuToTotalNumMap.get(productSku.getId()));
        });

        // 4.商品的详情图片
        ProductDetails productDetails = productDetailsMapper.selectOne(new LambdaQueryWrapper<ProductDetails>().eq(ProductDetails::getProductId, id));
        product.setDetailsImageUrlList(Arrays.asList(productDetails.getImageUrls().split(",")));

        return product;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateProduct(Product product) {
        // 1.修改商品信息
        productMapper.updateById(product);

        List<ProductSku> productSkuList = product.getProductSkuList();
        if (productSkuList != null) {
            productSkuList.forEach(productSku -> {
                // 2.修改SKU信息
                productSkuMapper.updateById(productSku);

                // 3.修改库存信息
                SkuStock skuStock = skuStockMapper.selectOne(new LambdaQueryWrapper<SkuStock>().eq(SkuStock::getSkuId, productSku.getId()));
                if (skuStock != null) {
                    skuStock.setTotalNum(productSku.getStockNum());
                    skuStock.setAvailableNum(skuStock.getTotalNum() - skuStock.getLockNum());
                    skuStockMapper.updateById(skuStock);
                }
            });
        }

        // 4.修改商品详细信息
        ProductDetails productDetails = productDetailsMapper.selectOne(new LambdaQueryWrapper<ProductDetails>().eq(ProductDetails::getProductId, product.getId()));
        if (productDetails != null) {
            List<String> detailsImageUrlList = product.getDetailsImageUrlList();
            if (detailsImageUrlList == null) {
                detailsImageUrlList = new ArrayList<>();
            }
            productDetails.setImageUrls(String.join(",", detailsImageUrlList));
            productDetailsMapper.updateById(productDetails);
        }

        return 1;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteProductByIds(Long[] ids) {
        // 1.删除product
        productMapper.deleteBatchIds(Arrays.asList(ids));

        // 2.删除Sku信息
        List<ProductSku> productSkuList =
                productSkuMapper.selectList(new LambdaQueryWrapper<ProductSku>().in(ProductSku::getProductId, Arrays.asList(ids)).select(ProductSku::getId));
        List<Long> skuIdList = productSkuList.stream().map(ProductSku::getId).collect(Collectors.toList());
        productSkuMapper.delete(new LambdaQueryWrapper<ProductSku>().in(ProductSku::getProductId, ids));

        // 3.删除库存信息
        skuStockMapper.delete(new LambdaQueryWrapper<SkuStock>().in(SkuStock::getSkuId, skuIdList));

        // 4.删除商品的详情图片
        productDetailsMapper.delete(new LambdaQueryWrapper<ProductDetails>().in(ProductDetails::getProductId, ids));

        return 1;

        // // 1.删除product
        // productMapper.deleteBatchIds(Arrays.asList(ids));
        // // 2.删除Sku信息
        // List<ProductSku> productSkuList 
        //         = productSkuMapper.selectList(new LambdaQueryWrapper<ProductSku>().in(ProductSku::getProductId, ids).select(ProductSku::getId));
        // List<Long> skuIdList = productSkuList.stream().map(ProductSku::getId).collect(Collectors.toList());
        // productSkuMapper.delete(new LambdaQueryWrapper<ProductSku>().in(ProductSku::getProductId, ids));
        // // 3.删除库存信息
        // skuStockMapper.delete(new LambdaQueryWrapper<SkuStock>().in(SkuStock::getSkuId, skuIdList));
        // // 4.删除商品的详情图片
        // productDetailsMapper.delete(new LambdaQueryWrapper<ProductDetails>().in(ProductDetails::getProductId, ids));
        // return 1;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAuditStatus(Long id, Integer auditStatus) {
        Product product = productMapper.selectOne(new LambdaQueryWrapper<Product>().eq(Product::getId, id));

        if (product == null) {
            throw new RuntimeException("产品不存在，无法更新审核状态");
        }

        if (auditStatus == 1) {
            product.setAuditStatus(1);
            product.setAuditMessage("审批通过");
        } else {
            product.setAuditStatus(-1);
            product.setAuditMessage("审批拒绝");
        }

        productMapper.updateById(product);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateStatus(Long id, Integer status) {
        Product product = productMapper.selectOne(new LambdaQueryWrapper<Product>().eq(Product::getId, id));

        if (product == null) {
            throw new RuntimeException("产品不存在");
        }

        if (status == 1) {
            product.setStatus(1);
        } else {
            product.setStatus(-1);
        }

        productMapper.updateById(product);
    }

    @Override
    public List<ProductSku> getTopSale() {
        return productSkuMapper.getTopSale();
    }
}