package com.spzx.product.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spzx.product.api.domain.vo.CategoryVo;
import com.spzx.product.domain.Category;
import com.spzx.product.domain.vo.CategoryExcelVo;
import com.spzx.product.mapper.CategoryMapper;
import com.spzx.product.service.ICategoryService;
import com.spzx.product.util.CategoryHelper;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 商品分类Service业务层处理
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<Category> treeSelect(Long parentId) {
        List<Category> categoryList = categoryMapper.selectList(new LambdaQueryWrapper<Category>().eq(Category::getParentId, parentId));
        if (!CollectionUtils.isEmpty(categoryList)) {
            categoryList.forEach(item -> {
                long count = categoryMapper.selectCount(new LambdaQueryWrapper<Category>().eq(Category::getParentId, item.getId()));
                if (count > 0) {
                    item.setHasChildren(true);
                } else {
                    item.setHasChildren(false);
                }
            });
        }
        return categoryList;
    }

    @Override
    public List<Long> getAllParentId(Long id) {
        List<Long> idList = new ArrayList<>();

        List<Category> list = this.getParentIdList(id, new ArrayList<Category>());
        for (int i = list.size() - 1; i >= 0; i--) {
            idList.add(list.get(i).getId());
        }

        return idList;
    }

    private List<Category> getParentIdList(Long id, List<Category> categoryList) {
        while (id > 0) {
            Category category = categoryMapper.selectById(id);
            categoryList.add(category);
            return getParentIdList(category.getParentId(), categoryList);
        }
        return categoryList;
    }

    @Override
    public void exportData(HttpServletResponse response) {
        try {
            // 1.设置响应结果类型和编码格式
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");

            // 2.这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("分类数据", "utf-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");

            // 3.查询数据库中的数据
            List<Category> categoryList = categoryMapper.selectList(null);

            // 4.创建存放 Category 的容器
            ArrayList<CategoryExcelVo> categoryExcelVoList = new ArrayList<>();

            // 5.类型转换，将查询出的数据存放在 VoList 中
            for (Category category : categoryList) {
                CategoryExcelVo categoryExcelVo = new CategoryExcelVo();
                BeanUtils.copyProperties(category, categoryExcelVo);
                categoryExcelVoList.add(categoryExcelVo);
            }
            // 6.写数据到客户端
            EasyExcel.write(response.getOutputStream(), CategoryExcelVo.class)
                    .sheet("分类数据")
                    .doWrite(categoryExcelVoList);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void importData(MultipartFile file) {
        try {
            List<CategoryExcelVo> categoryExcelVoList = EasyExcel.read(file.getInputStream())
                    .head(CategoryExcelVo.class)
                    .sheet()
                    .doReadSync();

            ArrayList<Category> categoryList = new ArrayList<>();
            for (CategoryExcelVo categoryExcelVo : categoryExcelVoList) {
                Category category = new Category();
                BeanUtils.copyProperties(categoryExcelVo, category);
                categoryList.add(category);
            }
            super.saveBatch(categoryList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<CategoryVo> getOneCategory() {
        List<Category> categoryList = categoryMapper.selectList(new LambdaQueryWrapper<Category>().eq(Category::getParentId, 0L));
        // List<CategoryVo> categoryVoList = new ArrayList<>();
        // for (Category category : categoryList) {
        //     CategoryVo categoryVo = new CategoryVo();
        //     BeanUtils.copyProperties(category, categoryVo);
        //     categoryVoList.add(categoryVo);
        // }
        return categoryList.stream().map(item -> {
            CategoryVo categoryVo = new CategoryVo();
            BeanUtils.copyProperties(item, categoryVo);
            return categoryVo;
        }).collect(Collectors.toList());
    }

    @Override
    public List<CategoryVo> tree() {
        List<Category> categoryList = categoryMapper.selectList(null);
        List<CategoryVo> categoryVoList = categoryList.stream().map(category -> {
            CategoryVo categoryVo = new CategoryVo();
            BeanUtils.copyProperties(category, categoryVo);
            return categoryVo;
        }).collect(Collectors.toList());
        
        return CategoryHelper.buildTree(categoryVoList);
    }
}