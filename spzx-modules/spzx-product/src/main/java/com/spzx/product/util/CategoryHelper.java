package com.spzx.product.util;

import com.spzx.product.api.domain.vo.CategoryVo;

import java.util.ArrayList;
import java.util.List;

public class CategoryHelper {

    public static List<CategoryVo> buildTree(List<CategoryVo> categoryVoList) {
        List<CategoryVo> oneCategoryList = new ArrayList<CategoryVo>();
        categoryVoList.forEach(categoryVo -> {
            if (categoryVo.getParentId() == 0) {
                oneCategoryList.add(findChildren(categoryVo, categoryVoList));
            }
        });
        return oneCategoryList;
    }

    /**
     * 递归找孩子
     *
     * @param categoryVo     父分类对象
     * @param categoryVoList 全部分类列表
     * @return 父分类对象
     */
    private static CategoryVo findChildren(CategoryVo categoryVo, List<CategoryVo> categoryVoList) {
        categoryVo.setChildren(new ArrayList<CategoryVo>());
        categoryVoList.forEach(item -> {
            if (categoryVo.getId() == item.getParentId()) {
                categoryVo.getChildren().add(findChildren(item, categoryVoList));
            }
        });

        return categoryVo;
    }
}
