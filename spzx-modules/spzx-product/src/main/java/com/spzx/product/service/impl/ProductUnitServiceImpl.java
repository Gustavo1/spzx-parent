package com.spzx.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spzx.product.domain.ProductUnit;
import com.spzx.product.mapper.ProductUnitMapper;
import com.spzx.product.service.IProductUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * 商品单位Service业务层处理
 */
@Service
public class ProductUnitServiceImpl extends ServiceImpl<ProductUnitMapper, ProductUnit> implements IProductUnitService {

    @Autowired
    private ProductUnitMapper productUnitMapper;

    @Override
    public IPage<ProductUnit> findPage(Page<ProductUnit> pageParam, ProductUnit productUnit) {
        // return productUnitMapper.findPage(pageParam, productUnit);
        LambdaQueryWrapper<ProductUnit> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (productUnit.getName() != null && !productUnit.getName().isEmpty()) {
            lambdaQueryWrapper.like(ProductUnit::getName, productUnit.getName());
        }
        return productUnitMapper.selectPage(pageParam, lambdaQueryWrapper);
    }

    @Override
    public ProductUnit getProductUnitById(Integer id) {
        return productUnitMapper.selectById(id);
    }

    @Override
    public int insertProductUnit(ProductUnit productUnit) {
        return productUnitMapper.insert(productUnit);
    }

    @Override
    public int updateProductUnit(ProductUnit productUnit) {
        return productUnitMapper.updateById(productUnit);
    }

    @Override
    public int deleteProductUnitByIds(Long[] ids) {
        return productUnitMapper.deleteBatchIds(Arrays.asList(ids));
    }
}