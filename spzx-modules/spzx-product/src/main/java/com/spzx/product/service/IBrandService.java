package com.spzx.product.service;

import com.spzx.product.domain.Brand;

import java.util.List;

/* 
* 品牌Service接口
*  */
public interface IBrandService {
    
    /**
     * 品牌列表分页查询
     * @param brand 查询条件
     * @return 列表数据
     */
    List<Brand> selectBrandList(Brand brand);

    /**
     * 根据id查询品牌详细信息
     * @param id 主键
     * @return 实体对象
     */
    Brand selectBrandById(Long id);

    /**
     * 保存品牌
     * @param brand 新增品牌
     * @return 影响数量
     */
    int insertBrand(Brand brand);

    /**
     * 修改品牌
     * @param brand 品牌
     * @return 影响数量
     */
    int updateBrand(Brand brand);

    /**
     * 批量删除品牌
     * @param ids 需要删除的品牌主键集合
     * @return 结果
     */
    int deleteBrandByIds(Long[] ids);

    /**
     * 获取全部品牌
     * @return
     */
    List<Brand> selectBrandAll();
}
