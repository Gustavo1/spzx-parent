package com.spzx.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spzx.product.domain.SkuStock;

public interface SkuStockMapper extends BaseMapper<SkuStock> {
}
