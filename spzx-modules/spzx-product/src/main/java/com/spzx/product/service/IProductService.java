package com.spzx.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spzx.product.domain.Product;
import com.spzx.product.api.domain.ProductSku;

import java.util.List;

/**
 * 商品Service接口
 */
public interface IProductService extends IService<Product> {

    /**
     * 查询商品列表
     *
     * @param product 商品
     * @return 商品集合
     */
    List<Product> selectProductList(Product product);

    int insertProduct(Product product);

    Product selectProductById(Long id);

    /**
     * 修改商品
     *
     * @param product 商品
     * @return 结果
     */
    int updateProduct(Product product);

    /**
     * 批量删除商品
     *
     * @param ids 需要删除的商品主键集合
     * @return 结果
     */
    int deleteProductByIds(Long[] ids);

    /**
     * 审批
     *
     * @param id          商品id
     * @param auditStatus 审核状态：0-初始值，1-通过，-1-未通过',
     */
    void updateAuditStatus(Long id, Integer auditStatus);

    /**
     * 上下架
     *
     * @param id     商品id
     * @param status 线上状态：0-初始值，1-上架，-1-自主下架',
     */
    void updateStatus(Long id, Integer status);

    /**
     * 获取销量好的sku（前20）
     *
     * @return
     */
    List<ProductSku> getTopSale();
}