package com.spzx.product.controller;

import com.spzx.common.core.web.controller.BaseController;
import com.spzx.common.core.web.domain.AjaxResult;
import com.spzx.common.core.web.page.TableDataInfo;
import com.spzx.common.security.annotation.RequiresPermissions;
import com.spzx.common.security.utils.SecurityUtils;
import com.spzx.product.domain.Brand;
import com.spzx.product.service.IBrandService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "品牌控制器类")
@RestController
@RequestMapping("/brand")
public class BrandController extends BaseController {

    @Autowired
    IBrandService brandService;

    @RequiresPermissions("product:brand:list")
    @Operation(summary = "品牌列表分页查询")
    @GetMapping("/list")
    public TableDataInfo list(Brand brand) {
        startPage();
        List<Brand> list = brandService.selectBrandList(brand);
        return getDataTable(list);
    }

    @RequiresPermissions("product:brand:query")
    @Operation(summary = "根据id查询品牌详细信息")
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable Long id) {
        Brand brand = brandService.selectBrandById(id);
        return AjaxResult.success(brand);
    }


    @RequiresPermissions("product:brand:add")
    @Operation(summary = "保存品牌")
    @PostMapping("/add")
    public AjaxResult add(@RequestBody @Validated Brand brand) {
        brand.setCreateBy(SecurityUtils.getUsername());
        return toAjax(brandService.insertBrand(brand));
    }

    @RequiresPermissions("product:brand:edit")
    @Operation(summary = "修改品牌")
    @PutMapping("/edit")
    public AjaxResult edit(@RequestBody @Validated Brand brand) {
        brand.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(brandService.updateBrand(brand));
    }

    @RequiresPermissions("product:brand:remove")
    @Operation(summary = "删除品牌")
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(brandService.deleteBrandByIds(ids));
    }

    @RequiresPermissions("product:brand:query")
    @Operation(summary = "获取全部品牌")
    @GetMapping("getBrandAll")
    public AjaxResult getBrandAll() {
        return success(brandService.selectBrandAll());
    }
}
