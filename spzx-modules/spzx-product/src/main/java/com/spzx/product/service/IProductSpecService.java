package com.spzx.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.spzx.common.core.web.domain.AjaxResult;
import com.spzx.product.domain.ProductSpec;

import java.util.List;

/**
 * 商品规格Service接口
 */
public interface IProductSpecService extends IService<ProductSpec> {


    /**
     * 查询商品规格列表
     * @param productSpec 商品规格
     * @return 商品规格集合
     */
    List<ProductSpec> selectProductSpecList(ProductSpec productSpec);

    /**
     * 根据id查询商品规格详细信息
     * @param id 商品规格id
     * @return  实体
     */
    ProductSpec selectProductById(Long id);

    List<ProductSpec> selectProductSpecListByCategoryId(Long categoryId);
}