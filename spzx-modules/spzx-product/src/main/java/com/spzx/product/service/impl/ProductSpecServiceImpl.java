package com.spzx.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.spzx.product.domain.ProductSpec;
import com.spzx.product.mapper.ProductSpecMapper;
import com.spzx.product.service.ICategoryService;
import com.spzx.product.service.IProductSpecService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品规格Service业务层处理
 */
@Service
public class ProductSpecServiceImpl extends ServiceImpl<ProductSpecMapper, ProductSpec> implements IProductSpecService {

    @Autowired
    private ProductSpecMapper productSpecMapper;

    @Autowired
    private ICategoryService categoryService;


    @Override
    public List<ProductSpec> selectProductSpecList(ProductSpec productSpec) {
        return productSpecMapper.selectProductSpecList(productSpec);
    }

    @Override
    public ProductSpec selectProductById(Long id) {
        ProductSpec productSpec = productSpecMapper.selectById(id);
        List<Long> ids = categoryService.getAllParentId(productSpec.getCategoryId());
        productSpec.setCategoryIdList(ids);
        return productSpec;
    }

    @Override
    public List<ProductSpec> selectProductSpecListByCategoryId(Long categoryId) {
        return productSpecMapper.selectList(new LambdaQueryWrapper<ProductSpec>()
                .eq(ProductSpec::getCategoryId, categoryId));
    }
}