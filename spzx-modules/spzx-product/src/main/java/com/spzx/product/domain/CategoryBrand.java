package com.spzx.product.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.spzx.common.core.web.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

/**
 * 分类品牌对象 category_brand
 */
@Data
public class CategoryBrand extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Schema(description = "品牌ID")
    @NotNull(message = "品牌ID不能为空")
    private Long brandId;

    @Schema(description = "分类ID")
    @NotNull(message = "分类ID不能为空")
    private Long categoryId;

    @Schema(description = "分类名称")
    @TableField(exist = false)
    private String categoryName;

    @Schema(description = "品牌名称")
    @TableField(exist = false)
    private String brandName;

    @Schema(description = "品牌图标")
    @TableField(exist = false)
    private String logo;

    // 用于修改表单页面，分类下拉回显。集合中分类id顺寻：一级、二级、三级
    @Schema(description = "封装当前分类id以及所有父级分类id")
    @TableField(exist = false)
    private List<Long> categoryIdList;

}