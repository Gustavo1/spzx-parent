package com.spzx.product.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.spzx.product.domain.ProductUnit;

public interface IProductUnitService extends IService<ProductUnit> {
    /**
     * 
     * @param pageParam
     * @param productUnit
     * @return
     */
    IPage<ProductUnit> findPage(Page<ProductUnit> pageParam, ProductUnit productUnit);

    ProductUnit getProductUnitById(Integer id);

    /**
     * 新增商品单位
     *
     * @param productUnit 商品单位
     * @return 结果
     */
    int insertProductUnit(ProductUnit productUnit);

    int updateProductUnit(ProductUnit productUnit);

    int deleteProductUnitByIds(Long[] ids);
}
