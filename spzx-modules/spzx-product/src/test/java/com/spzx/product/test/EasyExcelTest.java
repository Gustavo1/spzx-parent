package com.spzx.product.test;

import com.alibaba.excel.EasyExcel;
import com.spzx.product.SpzxProductApplication;
import com.spzx.product.domain.Category;
import com.spzx.product.domain.vo.CategoryExcelVo;
import com.spzx.product.listener.ExcelListener;
import com.spzx.product.service.ICategoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

// @SpringBootTest(classes = SpzxProductApplication.class)
@SpringBootTest
public class EasyExcelTest {

    @Autowired
    ICategoryService categoryService;

    @Test
    public void testExport() {
        // 1.查询数据库数据
        List<Category> categoryList = categoryService.list();

        // 2.类型转换
        ArrayList<CategoryExcelVo> categoryExcelVoList = new ArrayList<>();
        for (Category category : categoryList) {
            CategoryExcelVo categoryExcelVo = new CategoryExcelVo();
            BeanUtils.copyProperties(category, categoryExcelVo);
            categoryExcelVoList.add(categoryExcelVo);
        }

        // 3.生成导出Excel文件
        EasyExcel.write("D:/商品分类.xlsx", CategoryExcelVo.class)
                .sheet("分类")
                .doWrite(categoryExcelVoList);
    }

    @Test
    public void testImport() {
        // 创建一个泛型为 CategoryExcelVo 的 ExcelListener 实例，用于监听和处理 Excel 数据的读取
        ExcelListener<CategoryExcelVo> excelListener = new ExcelListener<>();

        // 使用 EasyExcel 读取指定路径的 Excel 文件，并指定映射的 Java 类为 CategoryExcelVo
        EasyExcel.read("D:/商品分类.xlsx") // 读取 Excel 文件路径
                .head(CategoryExcelVo.class) // 指定 Excel 文件中的数据要映射到的 Java 类
                .sheet() // 指定要读取的 sheet（这里默认读取第一个 sheet）
                .registerReadListener(excelListener) // 注册读取监听器，用于处理读取到的数据
                .doRead(); // 执行读取操作

        // 从监听器中获取读取到的数据列表
        List<CategoryExcelVo> data = excelListener.getData();
        // 创建一个用于存储 Category 对象的列表
        List<Category> categoryList = new ArrayList<>();

        // 遍历读取到的 CategoryExcelVo 对象
        for (CategoryExcelVo categoryExcelVo : data) {
            // 打印每个 CategoryExcelVo 对象的信息到控制台
            System.out.println("categoryExcelVo = " + categoryExcelVo);
            // 创建一个新的 Category 对象
            Category category = new Category();
            // 将 CategoryExcelVo 对象的属性拷贝到 Category 对象中
            BeanUtils.copyProperties(categoryExcelVo, category);
            // 将转换后的 Category 对象添加到 categoryList 列表中
            categoryList.add(category);
        }

        // 批量保存 Category 对象到数据库中
        categoryService.saveBatch(categoryList);
    }

}
