package com.spzx.product.api.factory;

import com.spzx.common.core.domain.R;
import com.spzx.product.api.RemoteCategoryService;
import com.spzx.product.api.domain.vo.CategoryVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RemoteCategoryFallbackFactory implements FallbackFactory<RemoteCategoryService> {

    private static final Logger log = LoggerFactory.getLogger(RemoteCategoryFallbackFactory.class);

    @Override
    public RemoteCategoryService create(Throwable cause) {
        log.error("商品分类服务调用降级" + cause.getMessage());
        return new RemoteCategoryService() {
            @Override
            public R<List<CategoryVo>> getOneCategory(String source) {
                // return R.fail(new ArrayList<CategoryVo>());
                return R.fail("获取全部一级分类失败:" + cause.getMessage());
            }

            @Override
            public R<List<CategoryVo>> tree(String source) {
                return R.fail("获取分类的数据结构失败:" + cause.getMessage());
            }
        };
    }
}
